﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoGrafos.DataStructure
{
    /// <summary>
    /// Classe que representa um grafo.
    /// </summary>
    public class Graph
    {

        #region Atributos

        /// <summary>
        /// Lista de nós que compõe o grafo.
        /// </summary>
        private List<Node> nodes;

        #endregion

        #region Propridades

        /// <summary>
        /// Mostra todos os nós do grafo.
        /// </summary>
        public Node[] Nodes
        {
            get { return this.nodes.ToArray(); }
        }

        #endregion

        #region Construtores

        /// <summary>
        /// Cria nova instância do grafo.
        /// </summary>
        public Graph()
        {
            this.nodes = new List<Node>();
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Encontra o nó através do seu nome.
        /// </summary>
        /// <param name="name">O nome do nó.</param>
        /// <returns>O nó encontrado ou nulo caso não encontre nada.</returns>
        private Node Find(string name)
        {
            foreach (Node item in this.nodes)
                if (item.Name == name)
                    return item;
            return null;
        }

        /// <summary>
        /// Adiciona um nó ao grafo.
        /// </summary>
        /// <param name="name">O nome do nó a ser adicionado.</param>
        /// <param name="info">A informação a ser armazenada no nó.</param>
        public void AddNode(string name)
        {
            nodes.Add(new Node(name, null));
        }

        /// <summary>
        /// Adiciona um nó ao grafo.
        /// </summary>
        /// <param name="name">O nome do nó a ser adicionado.</param>
        /// <param name="info">A informação a ser armazenada no nó.</param>
        public void AddNode(string name, object info)
        {
            nodes.Add(new Node(name, info));
        }

        /// <summary>
        /// Remove um nó do grafo.
        /// </summary>
        /// <param name="name">O nome do nó a ser removido.</param>
        public void RemoveNode(string name)
        {
            foreach (Node item in this.nodes)
                foreach (Edge ed in item.Edges)
                {
                    if (ed.To.Name == name)
                    {
                        Console.WriteLine("Nome: " + ed.To.Name);
                        item.Edges.Remove(ed);
                    }
                }
                    
            nodes.Remove(nodes.Find(node => node.Name == name));

        }

        /// <summary>
        /// Adiciona o arco entre dois nós associando determinado custo.
        /// </summary>
        /// <param name="from">O nó de origem.</param>
        /// <param name="to">O nó de destino.</param>
        /// <param name="cost">O cust associado.</param>
        public void AddEdge(string from, string to, double cost)
        {
            Node nodefrom = nodes.Find(n => n.Name == from);
            Node nodeto = nodes.Find(n => n.Name == from);
            nodefrom.AddEdge(nodeto, cost);
        }

        /// <summary>
        /// Obtem todos os nós vizinhos de determinado nó.
        /// </summary>
        /// <param name="node">O nó origem.</param>
        /// <returns></returns>
        public Node[] GetNeighbours(string from)
        {
            List<Node> neighbours = new List<Node>();

            foreach (Node nd in this.nodes)
            {
                foreach (Edge ed in nd.Edges)
                {
                    if (ed.From.Name == from)
                        neighbours.Add(ed.To);
                }
            }
            return neighbours.ToArray();
        }

        /// <summary>
        /// Valida um caminho, retornando a lista de nós pelos quais ele passou.
        /// </summary>
        /// <param name="nodes">A lista de nós por onde passou.</param>
        /// <param name="path">O nome de cada nó na ordem que devem ser encontrados.</param>
        /// <returns></returns>
        /// 
        public bool FindNextNode(ref List<Node> list, ref int i, string name)
        {
            foreach (Edge ed in list.Last().Edges)
                if (ed.To.Name == name)
                {
                    i++;
                    list.Add(ed.From);
                    return true;
                }
            return false;
        }

        public bool IsValidPath(ref Node[] nodes, params string[] path)
        {
            List<Node> nodelist = new List<Node>();
            bool flag = true;
            int i = 0;

            nodelist.Add(this.nodes.Find(node => node.Name == path[i]));

            i++;

            if (nodelist.Count != 0)
            {
                while(flag || i != path.Count())
                {
                    flag = FindNextNode(ref nodelist, ref i, path[i]);
                }
            }
            else
                return false;

            if (nodelist.Count() == path.Count())
            {
                nodes = nodelist.ToArray();
                return true;
            }
                
            return false;
        }

        #endregion

    }
}
